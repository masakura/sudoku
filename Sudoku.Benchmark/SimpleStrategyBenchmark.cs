﻿using BenchmarkDotNet.Attributes;
using Sudoku.Questions;

namespace Sudoku.Benchmark
{
    [RPlotExporter]
    public class SimpleStrategyBenchmark
    {
        private readonly Board _board = new Board().Load(Question.Load("sudoku.json"));
        private readonly SimpleStrategy _strategy = new();

        [Benchmark]
        public void Run()
        {
            _strategy.解く(_board);
        }
    }
}