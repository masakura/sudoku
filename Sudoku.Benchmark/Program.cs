﻿using BenchmarkDotNet.Running;

namespace Sudoku.Benchmark
{
    internal static class Program
    {
        private static void Main()
        {
            BenchmarkRunner.Run<SimpleStrategyBenchmark>();
        }
    }
}