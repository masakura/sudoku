using System.IO;
using System.Text.Json;

namespace Sudoku.Questions
{
    public record Question(QuestionCellValue[] squares)
    {
        public static Question Load(string path)
        {
            var text = File.ReadAllText(path);
            return JsonSerializer.Deserialize<Question>(text);
        }
    }
}