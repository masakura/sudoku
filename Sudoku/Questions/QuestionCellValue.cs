namespace Sudoku.Questions
{
    public record QuestionCellValue(int x, int y, int value);
}