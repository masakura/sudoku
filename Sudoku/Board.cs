using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sudoku.Questions;

namespace Sudoku
{
    public sealed class Board
    {
        private readonly Cells _cells;

        public Board() : this(Cells.Empty())
        {
        }

        private Board(Cells cells)
        {
            _cells = cells;
            Rows = new RowCollection(this);
            Columns = new ColumnCollection(this);
            Squares = new SquareCollection(this);
        }

        internal RowCollection Rows { get; }
        internal ColumnCollection Columns { get; }
        internal SquareCollection Squares { get; }

        public Board Load(Question question)
        {
            var board = this;

            foreach (var cell in question.squares)
                board = board.おく(cell.x, cell.y, cell.value);

            return board;
        }

        private Board おく(int x, int y, int number)
        {
            return おく(CellPosition.Get(RowIndex.GetZeroBase(y), ColumnIndex.GetZeroBase(x)), Number.Get(number));
        }

        internal Board おく(CellPosition position, Number number)
        {
            return new(_cells.おく(position, number));
        }

        internal CellValue GetValue(CellPosition position)
        {
            return _cells.GetValue(position);
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            builder.Append("  ");
            foreach (var column in ColumnIndex.All) builder.Append($"{column} ");
            builder.AppendLine();

            foreach (var row in RowIndex.All)
            {
                builder.Append($"{row} ");

                foreach (var column in ColumnIndex.All)
                {
                    var value = GetValue(CellPosition.Get(row, column));
                    builder.Append($"{value} ");
                }

                builder.AppendLine();
            }

            return builder.ToString();
        }

        internal IEnumerable<CellPosition> おける場所()
        {
            return _cells.おける場所();
        }

        internal IEnumerable<Number> おける数(CellPosition position)
        {
            return Rows.Get(position.Row).おける数()
                .Intersect(Columns.Get(position.Column).おける数())
                .Intersect(Squares.Get(position).おける数());
        }
    }
}