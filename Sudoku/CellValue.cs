using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Sudoku
{
    internal readonly struct CellValue : IEquatable<CellValue>
    {
        private static readonly IEnumerable<CellValue> Values =
            Sudoku.Number.All.Select(n => new CellValue(n)).ToImmutableArray();

#pragma warning disable 8632
        private readonly Number _value;
#pragma warning restore 8632

#pragma warning disable 8632
        private CellValue(Number value)
#pragma warning restore 8632
        {
            _value = value;
        }

        public static CellValue Empty => default;

        public Number Value()
        {
            return _value;
        }

        public override string ToString()
        {
            if (_value.Equals(default)) return " ";
            return _value.ToString();
        }

        private static CellValue Number(int value)
        {
            return Values.ElementAt(value - 1);
        }

        public static CellValue Number(Number value)
        {
            return Number(value.Value);
        }

        public bool おいてある()
        {
            return !_value.Equals(default);
        }

        public bool Equals(CellValue other)
        {
            return _value.Equals(other._value);
        }

        public override bool Equals(object obj)
        {
            return obj is CellValue other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public static bool operator ==(CellValue left, CellValue right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(CellValue left, CellValue right)
        {
            return !left.Equals(right);
        }
    }
}