using System.Linq;

namespace Sudoku
{
    internal sealed class ColumnCollection
    {
        private readonly Board _board;

        public ColumnCollection(Board board)
        {
            _board = board;
        }

        public Block Get(ColumnIndex column)
        {
            var cells = RowIndex.All.Select(row => _board.GetValue(CellPosition.Get(row, column)));
            return new Block(cells);
        }
    }
}