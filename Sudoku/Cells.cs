﻿using System.Collections.Generic;
using System.Linq;

namespace Sudoku
{
    internal sealed class Cells
    {
        private readonly CellValue[,] _cells;

        private Cells(CellValue[,] cells)
        {
            _cells = cells;
        }

        public static Cells Empty()
        {
            var cells = new CellValue[ColumnIndex.Max.Value, RowIndex.Max.Value];
            foreach (var row in RowIndex.All)
            foreach (var column in ColumnIndex.All)
                cells[column.ZeroIndex, row.ZeroIndex] = CellValue.Empty;

            return new Cells(cells);
        }

        public Cells おく(CellPosition position, Number number)
        {
            var clone = (CellValue[,]) _cells.Clone();
            clone[position.Column.ZeroIndex, position.Row.ZeroIndex] = CellValue.Number(number);
            return new Cells(clone);
        }

        public CellValue GetValue(CellPosition position)
        {
            return _cells[position.Column.ZeroIndex, position.Row.ZeroIndex];
        }

        public IEnumerable<CellPosition> おける場所()
        {
            return CellPosition.All().Where(おける);
        }

        private bool おける(CellPosition position)
        {
            var value = _cells[position.Column.ZeroIndex, position.Row.ZeroIndex];
            return !value.おいてある();
        }
    }
}