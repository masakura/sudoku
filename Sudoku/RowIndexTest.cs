﻿using System.Linq;
using PowerAssert;
using Xunit;

namespace Sudoku
{
    public sealed class RowIndexTest
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void TestRowRange1(int row)
        {
            var actual = RowIndex.Get(row).SquareRange().ToArray();

            PAssert.IsTrue(() => actual.SequenceEqual(new[] {1, 2, 3}));
        }

        [Theory]
        [InlineData(4)]
        [InlineData(5)]
        [InlineData(6)]
        public void TestRowRange2(int row)
        {
            var actual = RowIndex.Get(row).SquareRange().ToArray();

            PAssert.IsTrue(() => actual.SequenceEqual(new[] {4, 5, 6}));
        }
    }
}