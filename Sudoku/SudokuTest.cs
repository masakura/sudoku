﻿using PowerAssert;
using Sudoku.Questions;
using Xunit;

namespace Sudoku
{
    public sealed class SudokuTest
    {
        private readonly Board _board;
        private readonly SimpleStrategy _target;

        public SudokuTest()
        {
            _board = new Board().Load(Question.Load("test.json"));
            _target = new SimpleStrategy();
        }

        [Fact]
        public void 解く()
        {
            var (_, result) = _target.解く(_board);

            PAssert.IsTrue(() => result);
        }
    }
}