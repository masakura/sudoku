using System.Linq;

namespace Sudoku
{
    internal sealed class RowCollection
    {
        private readonly Board _board;

        public RowCollection(Board board)
        {
            _board = board;
        }

        public Block Get(RowIndex row)
        {
            var cells = ColumnIndex.All.Select(column => _board.GetValue(CellPosition.Get(row, column)));
            return new Block(cells);
        }
    }
}