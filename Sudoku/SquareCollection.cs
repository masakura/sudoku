using System.Collections.Generic;

namespace Sudoku
{
    internal sealed class SquareCollection
    {
        private readonly Board _board;

        public SquareCollection(Board board)
        {
            _board = board;
        }

        public Block Get(CellPosition position)
        {
            return new(Positions(position));
        }

        private IEnumerable<CellValue> Positions(CellPosition position)
        {
            foreach (var row in position.Row.SquareRange())
            foreach (var column in position.Column.SquareRange())
                yield return _board.GetValue(CellPosition.Get(row, column));
        }
    }
}