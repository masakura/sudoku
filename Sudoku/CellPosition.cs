﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sudoku
{
    internal readonly struct CellPosition : IEquatable<CellPosition>
    {
        private static readonly CellPosition[,] Positions;

        static CellPosition()
        {
            Positions = new CellPosition[ColumnIndex.Max.Value, RowIndex.Max.Value];
            foreach (var row in RowIndex.All)
            foreach (var column in ColumnIndex.All)
                Positions[column.ZeroIndex, row.ZeroIndex] = new CellPosition(row, column);
        }

        private CellPosition(RowIndex row, ColumnIndex column)
        {
            Row = row;
            Column = column;
        }

        public ColumnIndex Column { get; }
        public RowIndex Row { get; }

        public static CellPosition Get(RowIndex row, ColumnIndex column)
        {
            return Positions[column.ZeroIndex, row.ZeroIndex];
        }

        public static CellPosition Get(int row, int column)
        {
            return Positions[column - 1, row - 1];
        }

        public static IEnumerable<CellPosition> All()
        {
            return Positions.Cast<CellPosition>();
        }

        public bool Equals(CellPosition other)
        {
            return Column.Equals(other.Column) && Row.Equals(other.Row);
        }

        public override bool Equals(object obj)
        {
            return obj is CellPosition other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Column, Row);
        }

        public static bool operator ==(CellPosition left, CellPosition right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(CellPosition left, CellPosition right)
        {
            return !left.Equals(right);
        }
    }
}