using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Sudoku
{
    internal readonly struct RowIndex : IEquatable<RowIndex>
    {
        private readonly Index _index;

        private RowIndex(Index index)
        {
            _index = index;
        }

        public int Value => _index.Value;
        public int ZeroIndex => _index.ZeroIndex;

        public static IEnumerable<RowIndex> All { get; } =
            Index.All.Select(i => new RowIndex(i)).ToImmutableArray();

        public static RowIndex Max { get; } = new(Index.Max);

        public static RowIndex Get(int value)
        {
            return All.ElementAt(value - 1);
        }

        public static RowIndex GetZeroBase(int value)
        {
            return All.ElementAt(value);
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public IEnumerable<RowIndex> SquareRange()
        {
            return _index.SquareRange().Select(index => new RowIndex(index));
        }

        public bool Equals(RowIndex other)
        {
            return _index.Equals(other._index);
        }

        public override bool Equals(object obj)
        {
            return obj is RowIndex other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _index.GetHashCode();
        }

        public static bool operator ==(RowIndex left, RowIndex right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(RowIndex left, RowIndex right)
        {
            return !left.Equals(right);
        }
    }
}