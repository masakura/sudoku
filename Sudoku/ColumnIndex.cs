using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Sudoku
{
    internal readonly struct ColumnIndex : IEquatable<ColumnIndex>
    {
        private readonly Index _index;

        private ColumnIndex(Index index)
        {
            _index = index;
        }

        public static IEnumerable<ColumnIndex> All { get; } =
            Index.All.Select(i => new ColumnIndex(i)).ToImmutableArray();

        public int Value => _index.Value;
        public int ZeroIndex => _index.ZeroIndex;
        public static ColumnIndex Max { get; } = new(Index.Max);

        public override string ToString()
        {
            return Value.ToString();
        }

        public IEnumerable<ColumnIndex> SquareRange()
        {
            return _index.SquareRange().Select(index => new ColumnIndex(index));
        }

        public static ColumnIndex Get(int value)
        {
            return All.ElementAt(value - 1);
        }

        public static ColumnIndex GetZeroBase(int value)
        {
            return All.ElementAt(value);
        }

        public bool Equals(ColumnIndex other)
        {
            return _index.Equals(other._index);
        }

        public override bool Equals(object obj)
        {
            return obj is ColumnIndex other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _index.GetHashCode();
        }

        public static bool operator ==(ColumnIndex left, ColumnIndex right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(ColumnIndex left, ColumnIndex right)
        {
            return !left.Equals(right);
        }
    }
}