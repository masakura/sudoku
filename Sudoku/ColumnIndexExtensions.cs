﻿using System.Collections.Generic;
using System.Linq;

namespace Sudoku
{
    internal static class ColumnIndexExtensions
    {
        public static bool SequenceEqual(this IEnumerable<ColumnIndex> first, IEnumerable<int> second)
        {
            return first.SequenceEqual(second.Select(ColumnIndex.Get).ToArray());
        }
    }
}