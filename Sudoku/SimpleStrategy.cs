using System.Linq;

namespace Sudoku
{
    public sealed class SimpleStrategy
    {
        public (Board, bool) 解く(Board board)
        {
            var おける場所 = board.おける場所().FirstOrDefault();
            if (おける場所 == null) return (board, true);

            foreach (var number in board.おける数(おける場所))
            {
                var newBoard = board.おく(おける場所, number);
                var (board2, result) = 解く(newBoard);
                if (result) return (board2, true);
            }

            return (board, false);
        }
    }
}