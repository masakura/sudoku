﻿using System.Collections.Generic;
using System.Linq;

namespace Sudoku
{
    internal static class RowIndexExtensions
    {
        public static bool SequenceEqual(this IEnumerable<RowIndex> first, IEnumerable<int> second)
        {
            return first.SequenceEqual(second.Select(RowIndex.Get).ToArray());
        }
    }
}