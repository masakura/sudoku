﻿using System.Linq;
using PowerAssert;
using Xunit;

namespace Sudoku
{
    public sealed class ColumnIndexTest
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void TestColumnRange1(int column)
        {
            var actual = ColumnIndex.Get(column).SquareRange().ToArray();

            PAssert.IsTrue(() => actual.SequenceEqual(new[] {1, 2, 3}));
        }
    }
}