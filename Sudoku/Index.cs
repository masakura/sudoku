using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Sudoku
{
    internal readonly struct Index : IEquatable<Index>
    {
        private Index(int value)
        {
            Value = value;
            ZeroIndex = value - 1;
        }

        public int Value { get; }
        public int ZeroIndex { get; }

        public static IEnumerable<Index> All { get; } =
            Enumerable.Range(1, 9).Select(n => new Index(n)).ToImmutableArray();

        public static Index Max => All.Last();

        public override string ToString()
        {
            return Value.ToString();
        }

        public IEnumerable<Index> SquareRange()
        {
            var value = (Value - 1) / 3 * 3 + 1;

            yield return new Index(value);
            yield return new Index(value + 1);
            yield return new Index(value + 2);
        }

        public bool Equals(Index other)
        {
            return Value == other.Value && ZeroIndex == other.ZeroIndex;
        }

        public override bool Equals(object obj)
        {
            return obj is Index other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value, ZeroIndex);
        }

        public static bool operator ==(Index left, Index right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Index left, Index right)
        {
            return !left.Equals(right);
        }
    }
}