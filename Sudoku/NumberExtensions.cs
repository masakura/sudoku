﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sudoku
{
    internal static class NumberExtensions
    {
        [Obsolete]
        public static Number[] ToNumbers(this IEnumerable<int> numbers)
        {
            return numbers.Select(n => Number.Get(n)).ToArray();
        }

        public static bool SequenceEqual(this IEnumerable<Number> first, IEnumerable<int> second)
        {
            return first.SequenceEqual(second.Select(Number.Get).ToArray());
        }
    }
}