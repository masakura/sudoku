using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Sudoku
{
    internal sealed class Block
    {
        private readonly ImmutableArray<Number> _おいてある数;

        public Block(IEnumerable<CellValue> values)
        {
            _おいてある数 = values
                .Where(value => value.おいてある())
                .Select(value => value.Value())
                .ToImmutableArray();
        }

        public IEnumerable<Number> おける数()
        {
            return Number.All.Except(_おいてある数);
        }
    }
}