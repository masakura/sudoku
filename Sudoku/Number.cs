using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Sudoku
{
    internal readonly struct Number : IEquatable<Number>
    {
        private Number(int value)
        {
            Value = value;
        }

        public int Value { get; }

        public static IEnumerable<Number> All { get; } =
            Enumerable.Range(1, 9).Select(n => new Number(n)).ToImmutableArray();

        public static Number Get(int value)
        {
            return All.ElementAt(value - 1);
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public bool Equals(Number other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is Number other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value;
        }

        public static bool operator ==(Number left, Number right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Number left, Number right)
        {
            return !left.Equals(right);
        }
    }
}