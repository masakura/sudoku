using System.Linq;
using PowerAssert;
using Sudoku.Questions;
using Xunit;
using Xunit.Abstractions;

namespace Sudoku
{
    public sealed class BoardTest
    {
        private readonly Board _board;
        private readonly ITestOutputHelper _output;

        public BoardTest(ITestOutputHelper output)
        {
            _output = output;

            _board = new Board().Load(Question.Load("test.json"));
        }

        [Fact]
        public void ボードを表示する()
        {
            _output.WriteLine(_board.ToString());
        }

        [Fact]
        public void おける数行()
        {
            var result = _board.Rows.Get(RowIndex.Get(1)).おける数().ToArray();

            PAssert.IsTrue(() => result.SequenceEqual(new[] {1, 2, 3, 4, 5, 6, 7, 9}));
        }

        [Fact]
        public void おける数正方形()
        {
            var actual = _board.Squares.Get(CellPosition.Get(1, 1)).おける数().ToArray();

            PAssert.IsTrue(() => actual.SequenceEqual(new[] {2, 3, 4, 5, 6, 7, 9}));
        }

        [Fact]
        public void おける数()
        {
            var actual = _board.おける数(CellPosition.Get(1, 2)).ToArray();

            PAssert.IsTrue(() => actual.SequenceEqual(new[] {2, 3, 4, 5, 6, 7, 9}));
        }

        [Fact]
        public void おける場所()
        {
            var actual = _board.おける場所().First();

            PAssert.IsTrue(() => actual == CellPosition.Get(1, 1));
        }
    }
}